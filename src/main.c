#include "configs.h"

#include <sys_clk.h>
#include <gpio.h>
#include <uart.h>
#include <adc.h>

#include <MDR32Fx.h>


int main(void)
{
    GPIO_Init(&led1Port);
    GPIO_Init(&led2Port);
    GPIO_Init(&led3Port);
    GPIO_Init(&btnPort);

    GPIO_Init(&uart2RxPort);
    GPIO_Init(&uart2TxPort);

    SYS_CLK_TickInit(500, SYS_CLK_HSI_FREQ_HZ);
    SYS_CLK_SetHsiTrim(27);

    UART_Init(UART_Interface2, 115200, SYS_CLK_HSI_FREQ_HZ);

    const uint8_t adcDiv = 4;
    ADC_Init(adcDiv);

    while(1)
    {
        if(SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk)
        {
            GPIO_ToggleState(LED3_PORT, LED3_PIN);
            static uint32_t itrCnt = 0;
            const uint8_t adcChannel = 6;
            ADC_BeginConv(adcChannel);
            ADC_WaitReady();
            const uint16_t adcVal = ADC_GetResult();
            UART_Printf(UART_Interface2, "\r\nTest iteration %u | ADC-%u = %u", itrCnt, adcChannel, adcVal);
            itrCnt++;
        }

        static bool _btnFlag = false;

        if(!GPIO_GetState(BTN_PORT, BTN_PIN) && !_btnFlag)
        {
            static bool startFlag = false;

            if(!startFlag)
            {
                startFlag = true;
                uint32_t cpuFreqHz = SYS_CLK_Init(&sysClkConfig);
                UART_Init(UART_Interface2, 115200, cpuFreqHz);
                GPIO_SetState(LED1_PORT, LED1_PIN, true);
            }
            else
            {
                GPIO_ToggleState(LED1_PORT, LED1_PIN);
                GPIO_ToggleState(LED2_PORT, LED2_PIN);
                MDR_RST_CLK->CPU_CLOCK ^= RST_CLK_CPU_CLOCK_CPU_C2_SEL;
            }
            _btnFlag = true;
        }

        if(GPIO_GetState(BTN_PORT, BTN_PIN) && _btnFlag)
        {
            _btnFlag = false;
        }
    }
}

