#pragma once

#include <sys_clk.h>
#include <gpio.h>

#define CLK_HSE_FREQ_HZ             (16##000##000)

#define LED1_PORT                   (GPIO_PortB)
#define LED1_PIN                    (1)
#define LED2_PORT                   (GPIO_PortB)
#define LED2_PIN                    (2)
#define LED3_PORT                   (GPIO_PortC)
#define LED3_PIN                    (2)
#define BTN_PORT                    (GPIO_PortD)
#define BTN_PIN                     (5)

static const SYS_CLK_Config_s sysClkConfig =
{
    .hsGenType      = SYS_CLK_HsGenTypeHse,
    .hsDiv2Enable   = false,
    .hseFreqHz      = CLK_HSE_FREQ_HZ,
    .pllMul         = 2,
    .divPwrOf2      = 0,
};

static const GPIO_Config_s led1Port =
{
    .port           = LED1_PORT,
    .pin            = LED1_PIN,
    .mode           = GPIO_ModeIo,
    .output         = true,
    .openDrain      = false,
    .driveStrength  = GPIO_DsNormal
};

static const GPIO_Config_s led2Port =
{
    .port           = LED2_PORT,
    .pin            = LED2_PIN,
    .mode           = GPIO_ModeIo,
    .output         = true,
    .openDrain      = false,
    .driveStrength  = GPIO_DsNormal
};

static const GPIO_Config_s led3Port =
{
    .port           = LED3_PORT,
    .pin            = LED3_PIN,
    .mode           = GPIO_ModeIo,
    .output         = true,
    .openDrain      = false,
    .driveStrength  = GPIO_DsNormal
};

static const GPIO_Config_s btnPort =
{
    .port           = BTN_PORT,
    .pin            = BTN_PIN,
    .mode           = GPIO_ModeIo,
    .output         = false,
};

static const GPIO_Config_s uart2RxPort =
{
    .port           = GPIO_PortF,
    .pin            = 0,
    .mode           = GPIO_ModeRedefFunc,
    .output         = false,
};

static const GPIO_Config_s uart2TxPort =
{
    .port           = GPIO_PortF,
    .pin            = 1,
    .mode           = GPIO_ModeRedefFunc,
    .output         = true,
    .openDrain      = false,
    .driveStrength  = GPIO_DsNormal
};
